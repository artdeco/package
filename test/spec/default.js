import { deepEqual, throws } from '@type.engineering/web-computing'
import Context from '../context'
// import package from '../../src'
import getPackage from '../../src/bins'

/** @type {TestSuite} */
const T = {
  context: Context,
  // 'is a function'() {
  //   equal(typeof package, 'function')
  // },
  'gets bins for env'() {
    const res = getPackage({
      package: 'src/bin/index.js',
    }, 'test-compile')
    const { BINS } = res
    deepEqual(BINS, {
      package:'compile/dist/paid/compile/bin/package.js',
    })
  },
  'gets standard bin'() {
    const def = {
      package: 'src/bin/index.js',
      'package-2': 'src/bin/index1.js',
    }
    const res = getPackage(def, null)
    const { BINS } = res
    deepEqual(BINS, def)
  },
  async'throws an error on unknown bin'() {
    const def = {
      package: 'src/bin/index.js',
      'package-2': 'src/bin/index1.js',
    }
    await throws({
      fn() {
        getPackage(def, 'test', {
          'test': 'random-package',
        })
      },
      code: 'MODULE_NOT_FOUND',
    })
  },
  async'when bin isn\'t in bin'() {
    const def = {
      'package-2': 'src/bin/index1.js',
    }
    const { BINS } = getPackage(def, 'test-luddites')
    deepEqual(BINS, {})
  },
}

export default T