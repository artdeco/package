import { makeTestSuite } from '@type.engineering/web-computing'
import { EOL } from 'os'
import Context from '../context'
// import package from '../../src'

// export default
makeTestSuite('test/result/default', {
  /**
   * @param {Context} ctx
   */
  async getResults({ fixture, readFile }) {
    const text = readFile(fixture`${this.input}.txt`)
    const res = await package({
      text,
    })
    return `${this.input}:${EOL}${res}`
  },
  context: Context,
})