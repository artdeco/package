const { EOL } = require('os');
const { c } = require('@artdeco/erte');
const frameOfMind = require('frame-of-mind');
const { join, resolve } = require('path');

let TESTING_FREE = false

/**
 * Allows to resolve fields from a target package.
 * @param {!Object<string, string>} bins Names of binaries to resolve paths for. The value is the default bin when not operating on any environment, e.g., `{ 'my-package': 'src/bin/index.js' }`.
 * @param {string} [env] The environment. By default, reads ALAMODE_ENV.
 * @param {!Object<string,string>} [envs] Additional environments to scan.
 */
function getPackage(bins = {}, env = process.env.ALAMODE_ENV, envs = {}) {
  const error = (err) => {
    console.log('%s %s %s',
      c('⨯', 'red', { reverse: true }),
      env ? c(`[${env}]`, 'red', { reverse: true }) : '',
      c('An error occurred when getting environment bin:', 'red', { underline: true }))
  }
  const warn = (text = 'An warning when getting environment bin:') => {
    console.log('%s %s %s',
      c('⚠', 'yellow', { reverse: true }),
      env ? c(`[${env}]`, 'yellow', { reverse: true }) : '',
      c(text, 'yellow', { underline: true }),
    )
  }
  const ENVS = {
    'test-compile': 'compile/dist/paid',
    'test-npm': 'package/npm', // fuck npm
    'test-luddites': 'package/luddites',
    'test-lib': 'package/lib',
    ...envs,
  }
  /**
   * The relative path to the package within the CWD. Can be undefined.
   * @type {string|undefined}
   */
  const pckg = ENVS[env]
  if (pckg == 'package/npm') {
    TESTING_FREE = '[throws] '
  }

  let bin = {}, main, mod, BINS = {}
  if (pckg) {
    const j = join(pckg, 'package.json')
    try {
      ({
        'main': main = 'index.js',
        'module': mod = 'src/index.js',
        bin = {},
      } = require(resolve(pckg, 'package.json')))
    } catch (err) {
      error(err)
      if (err.code == 'MODULE_NOT_FOUND') {
        console.warn('%s %s',
          c('Could not require', 'red'),
          c(`${j}`, 'red', {
            reverse: true,
          }))
      }
      throw err
    }

    BINS = Object.entries(bins).reduce((acc, [k, b]) => {
      const value = bin[k]
      if (!value) {
        warn()
        console.error(
          '%s %s %s %s',
          c('Bin', 'red'),
          c(k, 'red', { reverse: true }),
          c(`isn't in`, 'red'),
          c(j, 'red', { reverse: true }),
        )
        console.warn('%s',
          c('The binary won\'t be available for testing.', 'red'))
        return acc
      }
      let BIN = join(pckg, value)
      acc[k] = BIN
      return acc
    }, {})
  } else {
    ({
      'main': main = 'index.js',
      'module': mod = 'src/index.js',
    } = require(resolve(process.cwd(), 'package.json')))
    BINS = bins
  }

  console.error(
    frameOfMind([
      c(`Testing ${pckg || 'src'} bin from`, 'blue', { underline: true }),
      ...Object.values(BINS).map((BIN) => {
        return c(BIN, 'blue', { reverse: true })
      }),
    ].join(EOL)))

  return { BINS, TESTING_FREE, main, mod, pckg }
}


module.exports = getPackage