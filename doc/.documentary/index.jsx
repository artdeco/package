import { repository } from '../../package.json'
import { format } from 'url'

/**
 * The footer for documentation.
 */
export const footer = () => {
  const alt = 'artdeco'
  const src = 'https://gitlab.com//uploads/-/system/group/avatar/7454762/artdeco.png&s=100'
  const href = 'https://www.artd.eco'
  const org = 'Art Deco™'
  const year = new Date().getFullYear()
  return [
    (<table>
      <tr>
        <td>
          <img src={src} alt={alt} />
        </td>
        <td>
          © <a href={href}>{org}</a> {year}
        </td>
      </tr>
    </table>),
  ]
}

const PipelineBadge = ({ version = 'master', alt = 'Pipeline Badge' }) => {
  const r = repository.replace('gitlab:', '')
  const badge = format({
    protocol: 'https',
    host: 'gitlab.com',
    pathname: `${r}/badges/${version}/pipeline.svg`
  })
  const commits = format({
    protocol: 'https',
    host: 'gitlab.com',
    pathname: `${r}/-/commits/${version}`
  })
  return `[![${alt}](${badge})](${commits})`
}

export const SECTION_BREAKS_URL = 'public://section-breaks'

export default {
  'pipeline-badge': PipelineBadge
}