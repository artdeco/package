## API

The package is available by importing its default function and named class:

```js
import package, { Package } from '@artdeco/package'
```

<include-types>types/api.xml</include-types>

%~ width="20"%

<function noArgTypesInToc level="3" name="package"/>

<example src="doc/example" />
<fork>doc/example</fork>

%~%

<typedef level="2" name="Package">types/index.xml</typedef>

%~%