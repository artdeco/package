<public gitlab dir="doc/pub" />
<example-override from="src" to="@artdeco/package" />
<div align="center"><p align="center">

# @artdeco/package

%NPM: @artdeco/package%
<pipeline-badge/>
</p></div>

`@artdeco/package` is: A Package Preset For Lud.

```sh
yarn add @artdeco/package
npm i @artdeco/package
```

## Table Of Contents

%TOC%

%~%