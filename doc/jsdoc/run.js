/* start example */
import { Package } from '../../'

/* end example */
;(async () => {
  /* start example */
  let package = new Package()
  let res = await package.run({
    shouldRun: true,
    text: 'hello-world',
  })
  console.log(res) // @artdeco/package called with hello-world
  /* end example */
})()