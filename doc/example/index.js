import package from '../../src'

(async () => {
  const res = await package({
    text: 'example',
  })
  console.log(res)
})()