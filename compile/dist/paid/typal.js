import { _package, _Package } from './compile/package'
require('./typedefs')

/** @api {_package.package} */
export default { _package }

/** @api {_package.Package} */
export { _Package }
