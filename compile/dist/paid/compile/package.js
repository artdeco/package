             
/*
 [@artdeco/]erte: String difference with colour and CLI ANSI formatting.

 Copyright (C) 2020  Art Deco

 This program is NOT free software: you cannot redistribute it and/or
 modify, reverse-engineer and embed into other programs under the terms
 of the Art Deco EULA which comes together with this distribution, and
 is available at artdeco.legal. You're allowed to use this program as
 a LIB with the license key issued per user.

*/
/*
 diff package https://github.com/kpdecker/jsdiff
 BSD License
 Copyright (c) 2009-2015, Kevin Decker <kpdecker@gmail.com>
*/
const k = {black:30, red:31, green:32, yellow:33, blue:34, magenta:35, cyan:36, white:37, grey:90}, l = {reset:0, bold:1, underline:4, i:5, h:6, reverse:7, conceal:8, f:9, g:10}, m = a => {
  Array.isArray(a) && (a = a.join(";"));
  return `\x1b[${a}m`;
};
function n(a) {
  const b = [k.yellow, ...Object.keys({}).map(c => l[c.toLowerCase()])].filter(Boolean);
  return b.length ? `${m(b)}${a}${m(l.reset)}` : a;
}
;async function p(a = {}) {
  const {shouldRun:b = !0, text:c = ""} = a;
  if (!b) {
    return "";
  }
  console.log("@artdeco/package called with %s", n(c));
  return c;
}
;const q = (a, b) => {
  a.prototype.a = b.reduce((c, {name:d, prototype:{a:g = []} = {}}) => {
    c.push(d, ...g);
    return c;
  }, [a.name]).filter(Boolean).filter((c, d, g) => g.indexOf(c) == d);
}, r = a => Object.entries(a).reduce((b, [c, d]) => {
  if (void 0 === d) {
    return b;
  }
  b[c] = d;
  return b;
}, {}), t = a => {
  var b = {};
  a = Object.getOwnPropertyDescriptors(a);
  Object.entries(a).forEach(([c, d]) => {
    const g = b[c];
    d = r(d);
    return b[c] = {...g, ...d};
  }, {});
  return b;
}, u = (a, b, c) => {
  if (a && a.constructor !== Object && (a = a.__proto__)) {
    var d = (Object.getOwnPropertyDescriptor(a, b) || {})[c];
    return d ? d : u(a, b, c);
  }
}, B = a => {
  const b = {};
  [...A(a.prototype), a.prototype].forEach(c => {
    c = t(c);
    Object.defineProperties(b, c);
  });
  return b;
}, A = (a, b = []) => {
  if (!a) {
    return b;
  }
  a = a.__proto__;
  if (!a || a.constructor === Object) {
    return b;
  }
  b.unshift(a);
  return A(a, b);
};
class C {
  constructor() {
    this.example = "ok";
  }
  async run() {
  }
}
;class D extends C {
  constructor() {
    super();
  }
}
(function(a, b, ...c) {
  var d = c.reduce((f, e) => {
    if ("function" == typeof e) {
      return f;
    }
    Object.setPrototypeOf(e, a.prototype);
    e = t(e);
    Object.defineProperties(f, e);
    return f;
  }, {}), g = c.map(f => "function" != typeof f ? null : B(f)).filter(Boolean);
  g.length && (g = [a.prototype, ...g].reduce((f, e) => {
    e = t(e);
    Object.defineProperties(f, e);
    return f;
  }, {}), g = Object.getOwnPropertyDescriptors(g), Object.defineProperties(a.prototype, g));
  d = Object.getOwnPropertyDescriptors(d);
  Object.entries(d).forEach(([f, e]) => {
    const v = e.value;
    var h;
    let w, x;
    if ("function" == typeof v) {
      const y = (Object.getOwnPropertyDescriptor(a.prototype, f) || {}).value;
      "function" == typeof y && (x = function(...z) {
        y.call(this, ...z);
        return v.call(this, ...z);
      });
    }
    e.get && !e.set ? h = u(b.prototype, f, "set") : e.set && !e.get && (w = u(b.prototype, f, "get"));
    h = r({value:x, set:h, get:w});
    Object.defineProperty(b.prototype, f, {...e, ...h});
  });
  q(b, c);
})(C, D, ...[function() {
}.prototype = {async run(a) {
  return await p(a);
}}]);
module.exports = {_package:p, _Package:D};


//# sourceMappingURL=package.js.map