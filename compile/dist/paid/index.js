const { _package, _Package } = require('./compile/package')
require('./typedefs')

/**
 * A Package Preset For Lud.
 * @param {!_package.package.Config} config Additional options for the program.
 * - `[shouldRun=true]` _boolean?_ A boolean option. Default `true`.
 * - `[text]` _string?_ A text to return.
 * @return {!Promise<string>} The result of processing.
 */
function $package(config) {
  return _package(config)
}

/**
 * A representation of a package.
 * @extends {_package.Package}
 * @typedef {Package}
 */
class Package extends _Package {}

module.exports = exports = $package
module.exports.Package = Package
