import '@luddites/lud'
import { format } from 'url'
import { dependencies } from '../package'
import { website } from './common'

export let name = '@artdeco/package'
export let description = 'A Package Preset For Lud.'

export let keywords = 'artdeco, package, lud, luddites'
export let author = 'Anton <anton@adc.sh>'
export let bugs = {
  url: 'https://gitlab.com/artdeco/package/-/issues',
  email: 'incoming+artdeco-package-22814977-issue-@incoming.gitlab.com'
}
export let homepage = format(website)

const SEE_LICENSE = 'SEE LICENSE IN EULA.md'
const EULA = 'node_modules/@artdeco/license/EULA.md'
const CHANGELOG = 'CHANGELOG.md'
const README = 'README.md'
const ECR = [EULA, CHANGELOG, README]

const TYPEDEFS = {
  'types/typedefs/index.js': 'typedefs.js',
}

/** @type {_lud.Env} */
export const LUDDITES = {
  description: `${description} [PAID]`,
  addFiles: [
    'build/index.js',
    'build/bins.js',
    'src/.alamoderc.json',
    ECR,
  ],
  dependencies,
  // ignoreSourceMaps: true,
  // copy: {
  //   ...TYPEDEFS,
  // },
  license: SEE_LICENSE,
  keywords: undefined,
}

// // work in progress
// export const LIB = {
//   module: 'src/index.js',
//   externs: 'types/externs.js',
//   addFiles: [
//     'package/EULA', 'src',
//     'types', 'example/jsdoc/',
//
//     ...compile,
//
//
//   ],
//   license: 'SEE LICENSE IN EULA',
//
// }


export const LOCAL_COMPILE = {
  version: `local-${process.env.VERSION}-compile`,
  copy: {
    ...TYPEDEFS,
  },
  bin: {
    'package': 'compile/bin/package.js',
  },
}