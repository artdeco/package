<div align="center"><p align="center">

# @artdeco/package

[![npm version](https://badge.fury.io/js/@artdeco/package.svg)](https://www.npmjs.com/package/@artdeco/package)
[![Pipeline Badge](https://gitlab.com/artdeco/package/nodejs/badges/master/pipeline.svg)](https://gitlab.com/artdeco/package/nodejs/-/commits/master)
</p></div>

`@artdeco/package` is: A Package Preset For Lud.

```sh
yarn add @artdeco/package
npm i @artdeco/package
```

## Table Of Contents

- [Table Of Contents](#table-of-contents)
- [API](#api)
- [`async package(config: !PackageConfig): !Promise<string>`](#async-mynewpackageconfig-mynewpackageconfig-promisestring)
  * [`PackageConfig`](#type-mynewpackageconfig)
- [`Package`](#type-mynewpackage)
- [CLI](#cli)
- [Copyright & License](#copyright--license)

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="https://artdeco.gitlab.io/package/nodejs/section-breaks/0.svg">
</a></p></div>

## API

The package is available by importing its default function and named class:

```js
import package, { Package } from '@artdeco/package'
```

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="https://artdeco.gitlab.io/package/nodejs/section-breaks/1.svg">
</a></p></div>

## <code>async <ins>package</ins>(</code><sub><br/>&nbsp;&nbsp;`config: !PackageConfig,`<br/></sub><code>): <i>!Promise&lt;string&gt;</i></code>

A Package Preset For Lud.

 - <kbd><strong>config*</strong></kbd> <em><code>![PackageConfig](#type-mynewpackageconfig "Additional options for the program.")</code></em>: Additional options for the program.

__<a name="type-mynewpackageconfig">`PackageConfig`</a>__: Additional options for the program.

|   Name    |   Type    |    Description    | Default |
| --------- | --------- | ----------------- | ------- |
| shouldRun | _boolean_ | A boolean option. | `true`  |
| text      | _string_  | A text to return. | -       |

```js
import package from '@artdeco/package'

(async () => {
  const res = await package({
    text: 'example',
  })
  console.log(res)
})()
```
```
@artdeco/package called with example
example
```

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="https://artdeco.gitlab.io/package/nodejs/section-breaks/2.svg">
</a></p></div>

__<a name="type-mynewpackage">`Package`</a>__: A representation of a package.

|      Name       |                                                               Type                                                               |             Description             | Initial |
| --------------- | -------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------- | ------- |
| __constructor__ | _new () =&gt; [Package](#type-mynewpackage "A representation of a package.")_                                               | Constructor method.                 | -       |
| __example__     | _string_                                                                                                                         | An example property.                | `ok`    |
| __run__         | _(conf: &#33;[PackageConfig](#type-mynewpackageconfig "Additional options for the program.")) =&gt; !Promise&lt;string&gt;_ | Executes main method via the class. | -       |

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="https://artdeco.gitlab.io/package/nodejs/section-breaks/3.svg">
</a></p></div>

## Copyright & License

SEE LICENSE IN LICENSE

<table>
  <tr>
    <td><img src="https://avatars3.githubusercontent.com/u/38815725?v=4&amp;s=100" alt="artdeco"></td>
    <td>© <a href="https://www.artd.eco">Art Deco™</a> 2020</td>
  </tr>
</table>

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="https://artdeco.gitlab.io/package/nodejs/section-breaks/-1.svg">
</a></p></div>