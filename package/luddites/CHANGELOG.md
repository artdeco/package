## 8 June 2024

### [0.8.0](https://gitlab.com/artdeco/package/compare/v0.7.1...v0.8.0)

- [feature] add method to update files easily.

## 22 February 2024

### [0.7.4](https://gitlab.com/artdeco/package/compare/v0.7.2...v0.7.4)

- [fix] don't peer-depend on `type.engineer.h`.
- [improvement] allow training paths for `src` remapping when testing.

## 18 March 2023

### [0.7.2](https://gitlab.com/artdeco/package/compare/v0.6.1...v0.7.2)

- [api] change `paid` to `dist/paid`.
- [feature] remove headers import.
- [feature] handle typology registrer.

## 21 October 2022

### [0.6.1](https://gitlab.com/artdeco/package/compare/v0.6.0...v0.6.1)

- [Fix] the renaming of all includes typedefs to `.h/typedefs`.

## 15 September 2022

### [0.6.1](https://gitlab.com/artdeco/package/compare/v0.5.3...v0.6.1)

- [Fix] removing the typal-marker from typedefs without new line.

## 16 October 2021

### [0.6.0](https://gitlab.com/artdeco/package/compare/v0.5.3...v0.6.0)

- [Feature] Add `installShared` method.

## 16 September 2021

### [0.5.3](https://gitlab.com/artdeco/package/compare/v0.5.0...v0.5.3)

- [LIB] Remove types from the lib.
- [Fix] Fix the build.
- [Fix] `if (PCKG.dependencies)`.

## 15 September 2021

### [0.5.0](https://gitlab.com/artdeco/package/compare/v0.4.0...v0.5.0)

- Add methods: `updateLibTypedefs`, `installLibTypeEngineer`,
  `installLudditesTypeEngineer`, `renameVendor` & `importsToRequire`.

## 09 July 2021

### [0.4.0](https://gitlab.com/artdeco/package/compare/v0.3.1...v0.4.0)

- [Feature] Add `removeTypalMeta` method to remove _Typal_ markers.

## 4 December 2020

### [0.3.1](https://gitlab.com/artdeco/package/compare/v0.3.0...v0.3.1)

- [fix] Return `pckg` field from bins also.

### [0.3.0](https://gitlab.com/artdeco/package/compare/v0.2.0...v0.3.0)

- [feature] Add binary resolution for inter-env testing.

## 3 December 2020

### [0.2.0](https://gitlab.com/artdeco/package/compare/v0.1.1...v0.2.0)

- [feature] Add `.alamoderc` to extend other packages' _ÀLaMode_ configs.

## 1 December 2020

### [0.1.1](https://gitlab.com/artdeco/package/compare/v0.1.0...v0.1.1)

- [fix] Don't use redundant typedefs in lib.

### 0.1.0

- Publish initial preset with lud config. Todo: alamoderc & scripts.json config.

### 0.0.0-pre

- Create `@artdeco/package` with _[`NodeTools`](https://art-deco.github.io/nodetools)_.