import { SUBTYPE_OPTS, Engineer, time, getSuper, initAbstract, assignSupers, subtype } from '@type.engineering/type-engineer'
import '../../types'

/** @extends {_package.Package} */
class _AbstractPackage {
  constructor() {
    /** @type {string} */ this.example = 'ok'
  }
}
initAbstract(_AbstractPackage, 'IPackage')
/**
 * A representation of a package.
 * @abstract
 * @implements {_package.IPackage}
 * @extends {_package.Package}
 */
export default class AbstractPackage extends _AbstractPackage {
  /**
   * Sets the implementation on the interface.
   * @param {...!(typeof Engineer|typeof AbstractPackage|Engineer|AbstractPackage)} Implementations
   * @suppress {checkPrototypalTypes}
   */
  static __implement(...Implementations) {
    const start = Date.now()
    const s = subtype(_AbstractPackage, SUBTYPE_OPTS, ...Implementations)
    assignSupers(_AbstractPackage, Implementations)
    time(_AbstractPackage, Date.now() - start)
    return s
  }
  get asIPackage() { return /** @type {!_package.BoundIPackage} */ (this) }
  get superIPackage() { return /** @type {!_package.BoundIPackage} */ (getSuper('IPackage', this)) }
}
AbstractPackage.__implement(Engineer)