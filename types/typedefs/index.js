/** @nocompile */
/** @const */
var _package = {}

/**
 * A representation of a package.
 * @interface
 */
_package.IPackage = class { }
/**
 * An example property. Default `ok`.
 * @type {string}
 */
_package.IPackage.prototype.example
/**
 * Cast the _IPackage_ instance into the _BoundIPackage_ type.
 * @type {!_package.BoundIPackage}
 */
_package.IPackage.prototype.asIPackage
/**
 * Access the _IPackage_ prototype.
 * @type {!_package.BoundIPackage}
 */
_package.IPackage.prototype.superIPackage
/** @type {typeof _package.IPackage.run} */
_package.IPackage.prototype.run = function() {}

/** @typedef {function(new: _package.IPackage)} _package.IPackage.constructor */
/** @typedef {typeof _package.IPackage} _package.IPackage.constructor.typeof */
/**
 * A constructor that constructs _IPackage_ instances.
 * @implements {_package.IPackage}
 * @constructor
 */
_package.Package = class extends /** @type {_package.IPackage.constructor&_package.IPackage.constructor.typeof} */ (class {}) { }
_package.Package.prototype = {}

/** @typedef {_package.IPackage} */
_package.RecordIPackage

/** @typedef {_package.IPackage} _package.BoundIPackage */

/** @typedef {typeof _package.IPackage.run} */
/**
 * Executes main method via the class.
 * @param {!_package.package.Config} conf The config to run the method against.
 * - `[shouldRun=true]` _boolean?_ A boolean option. Default `true`.
 * - `[text]` _string?_ A text to return.
 * @example
 * ```js
 * import { Package } from '@artdeco/package'
 *
 * let package = new Package()
 * let res = await package.run({
 *   shouldRun: true,
 *   text: 'hello-world',
 * })
 * console.log(res) // @artdeco/package called with hello-world
 * ```
 * @return {!Promise<string>} The result of transforms.
 */
_package.IPackage.run = function(conf) {}

/**
 * @typedef {Object} _package.package.Config Additional options for the program.
 * @prop {boolean} [shouldRun=true] A boolean option. Default `true`.
 * @prop {string} [text] A text to return.
 */
