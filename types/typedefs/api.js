/** @nocompile */
/** @const */
var _package = {}

/** @typedef {typeof _package.package} */
/**
 * A Package Preset For Lud.
 * @param {!_package.package.Config} config Additional options for the program.
 * - `[shouldRun=true]` _boolean?_ A boolean option. Default `true`.
 * - `[text]` _string?_ A text to return.
 * @return {!Promise<string>} The result of processing.
 */
_package.package = function(config) {}
