/**
 * @fileoverview
 * @externs
 */

/** @const */
var _package = {}

/** @interface */
_package.IPackage
/** @type {string} */
_package.IPackage.prototype.example
/** @type {!_package.BoundIPackage} */
_package.IPackage.prototype.asIPackage
/** @type {!_package.BoundIPackage} */
_package.IPackage.prototype.superIPackage
/**
 * @param {!_package.package.Config} conf
 * @return {!Promise<string>}
 */
_package.IPackage.prototype.run = function(conf) {}

/**
 * @implements {_package.IPackage}
 * @constructor
 */
_package.Package = function() {}

/** @typedef {{ run: typeof _package.IPackage.run, example: string, asIPackage: !_package.BoundIPackage, superIPackage: !_package.BoundIPackage }} */
_package.RecordIPackage

/**
 * @extends {_package.RecordIPackage}
 * @record
 */
_package.BoundIPackage = function() {}

/** @typedef {function(!_package.package.Config): !Promise<string>} */
_package.IPackage.run

/** @typedef {function(!_package.package.Config): !Promise<string>} */
_package.package

/** @record */
_package.package.Config = function() {}
/** @type {boolean|undefined} */
_package.package.Config.prototype.shouldRun
/** @type {string|undefined} */
_package.package.Config.prototype.text
