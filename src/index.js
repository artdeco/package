import { readFileSync, writeFileSync } from 'fs'
import { join } from 'path'

const EULA = 'node_modules/@artdeco/license/EULA.md'
const SEE_LICENSE = 'SEE LICENSE IN EULA.md'

const README = 'README.md'
const CHANGELOG = 'CHANGELOG.md'
const ECR = [EULA, CHANGELOG, README]

const TYPEDEFS = {
  'types/typedefs/index.js': 'types/typedefs.js',
}

export let license = SEE_LICENSE

/** @returns {_lud.Env} */
export function LUDDITES(description) {
  return {
    description: `${description} [PAID]`,
    addFiles: [
      'compile/dist/paid/compile',
      'compile/dist/paid/index.js',
      ECR,
    ],
    copy: {
      ...TYPEDEFS,
    },
    ignoreSourceMaps: true,
    dependencies: undefined,
    keywords: undefined,
  }
}

/**
 * @param {string} description
 * @param {!Object<string, string>} dependencies
 * @param {!_lud.Env} conf
 * @returns {_lud.Env}
 */
export function LIB(description, dependencies, conf) {
  const luddites = LUDDITES(description)
  return { ...luddites,
    ignoreSourceMaps: false,
    ignore: ['types/.typal.cache'],
    module: 'src/index.js',
    description: `${description} [SOURCE]`,
    addFiles: ['src', ...luddites.addFiles],
    dependencies,
    ...conf,
  }
}

export const LOCAL = {
  copy: {
    ...TYPEDEFS,
  },
  version: `local-${process.env.VERSION}`,
}

/**
 * @param {string} folder
 * @param  {...string} files
 */
export const removeTypalMeta = (folder, ...files) => {
  for (const file of files) {
    const path = join(folder, file)
    let r = readFileSync(path, 'utf-8')
    r = r.replace(/\/\* @typal-start {(.+?)} (.*?)(\S+) \*\/\r?\n/g, '')
    r = r.replace(/\/\* @typal-type .+? \*\/\r?\n/g, '')
    r = r.replace(/(\/\/.+?\r?\n)?\/\* @typal-end \*\/(\r?\n)*/g, '')
    writeFileSync(path, r.trim())
  }
}

/**
 * Updates references to typedefs in the lib to the .h package.
 * @param {string} name
 * @param {string} [INDEX]
 */
export const updateLibTypedefs=(name,typedefs,INDEX='package/lib/index.js') => {
  let r=readFileSync(INDEX,'utf-8')

  for(const sourcePath in typedefs) {
    const isNodeModule=sourcePath.startsWith('node_modules/')

    const targetPath=typedefs[sourcePath]
    const v=targetPath.replace('.js', '')

    let NEW_PATH=sourcePath
      .replace('node_modules/','')
      .replace('/index.js', '')
      .replace(/\.js$/, '')

    if(isNodeModule) {
      r=r.replace(`./${v}`, NEW_PATH)
    } else {
      NEW_PATH=NEW_PATH.replace(/types\//,'')
      r=r.replace(`./${v}`, `${name}.h/${NEW_PATH}`)
    }
  }

  r = r.replace('./types/typedefs', `${name}.h/typedefs`)
  writeFileSync(INDEX, r)
}

/**
 * Used for libs.
 * @param {Object} PCKG
 * @param {boolean} [withHeaders]
 */
export const installLibTypeEngineer = (PCKG,withHeaders) => {
  installShared(PCKG,'@type.engineering/type-engineer',withHeaders)
}
/**
 * Used for paid builds.
 * @param {Object} PCKG
 */
export const installLudditesTypeEngineer = (PCKG) => {
  installShared(PCKG,'@type.engineering/type-engineer')
}

/**
 * Makes sure that a dependency is installed as a peer dependency.
 * @param {Object} PCKG
 */
export function installShared(PCKG, pn, withHeaders=false) {
  const names=[pn]
  const hh = `${pn}.h`
  if(withHeaders)names.push(hh)
  const nn = {}
  for (const name of names) {
    nn[name]='*'
    if (PCKG.dependencies) {
      delete PCKG.dependencies[name]
      delete PCKG.dependencies[hh]
    }
  }
  PCKG.peerDependencies=PCKG.peerDependencies||{}
  Object.assign(PCKG.peerDependencies, nn)
}

/**
 * Goes through the vendor typedefs and comments them out.
 * @param {string} pckg
 */
export const renameVendor = (pckg) => {
  const VENDOR = `package/${pckg}/typedefs/vendor.js`
  let r = readFileSync(VENDOR, 'utf-8')
  r = r.replace(/import/g, '/'+'/ import')
  writeFileSync(VENDOR, r)
}

export const importsToRequire = (PATH) => {
  let r = readFileSync(PATH, 'utf-8')
  r = r.replace(/import ('.+')/g, 'require($1)')
  writeFileSync(PATH, r)
}

/**
 * Removes the import of a headers package.
 * @param {string} PCKG
 * @param {...string} PATHS
 */
export const removeH=(PCKG,...PATHS) => {
  for(let PATH of PATHS) {
    if(PATH.endsWith('/')) PATH=join(PATH,'index.js')
    const path=join(PCKG,PATH)
    let f=readFileSync(path,'utf-8')+''
    f=f.replace(/import '@.+?\.h'\s+/,'')
    writeFileSync(path,f)
  }
}

/**
 * Includes the typology registration file as common js module in the dist.
 * @param {string} DIST The path to the dist, e.g., `compile/dist/paid`.
 * @param {string} [typologyPath] The path to the typology map registrer.
 * Default `'types/db/typology.js'`.
 */
export const placeTypology=(DIST,typologyPath='types/db/typology.js')=>{
  let f=readFileSync(typologyPath,'utf-8')
  f=f.replace(/import\s*\{\s*registerTypology\s*\}\s*from\s*'@type.engineering\/type-engineer'/,
    'const{registerTypology}=require(\'@type.engineering/type-engineer\')')
  writeFileSync(join(DIST,'types/typology.js'),f)
}
/**
 *
 * @param {string} PCKG The package.
 */
export const removeTypology=(PCKG)=>{
  const path=join(PCKG,'index.js')
  let f=readFileSync(path,'utf-8')
  f=f.replace(/.+typology.+\s+/,'')
  writeFileSync(path,f)
}

/**
 * @param {string} path
 * @param {string|!RegExp} predicate
 * @param {string} string
 */
export function updateFile(path,predicate,result) {
  let d=readFileSync(path)+''
  d=d.replace(predicate,result)
  writeFileSync(path,d)
}